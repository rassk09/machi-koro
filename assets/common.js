// import Modal from 'modal';
// import Slider from 'slider';

let ww = $(window).width(),
    wh = $(window).height(),
    baseWidth = 1280,
    baseHeight = 720,
    cards = [
        {
            id: 1,
            type: 'blue',
            name: 'Пшеничное поле',
            cost: 1,
            dice: [1],
            playerHas: true,
            image: 'images/cards/blue/1_wheat_field.png',
            position: {
                x: 0,
                y: 0,
            },
            aiPriority: 2,
            usagePriority: 2,
            usage: function(player) {
                addCoin(player.id - 1);
            }
        },
        {
            id: 2,
            type: 'green',
            name: 'Пекарня',
            cost: 1,
            dice: [2, 3],
            playerHas: true,
            image: 'images/cards/green/2_3_bakery.png',
            position: {
                x: 1,
                y: 0,
            },
            aiPriority: 1,
            usagePriority: 2,
            usage: function(player) {
                if (currentPlayer == player.id - 1) {
                    var total_coins = 1;
                    if (player.flag_center) total_coins++;
                    var coins_delay = 0;
                    for (var i = 0; i < total_coins; i++) {
                        coins_delay += 50;
                        $.wait(coins_delay + 50, function(){
                            addCoin(player.id - 1);
                        });
                    }
                }
            }
        },
        {
            id: 3,
            type: 'blue',
            name: 'Ферма',
            cost: 1,
            dice: [2],
            playerHas: false,
            image: 'images/cards/blue/2_farm.png',
            position: {
                x: 2,
                y: 0,
            },
            aiPriority: 2,
            usagePriority: 2,
            usage: function(player) {
                addCoin(player.id - 1);
            }
        },
        {
            id: 4,
            type: 'green',
            name: 'Магазин',
            cost: 3,
            dice: [4],
            playerHas: false,
            image: 'images/cards/green/4_shop.png',
            position: {
                x: 4,
                y: 0,
            },
            aiPriority: 1,
            usagePriority: 2,
            usage: function(player) {
                if (currentPlayer == player.id - 1) {
                    var total_coins = 3;
                    if (player.flag_center) total_coins++;

                    var coins_delay = 0;
                    for (var i = 0; i < total_coins; i++) {
                        coins_delay += 50;
                        $.wait(coins_delay + 50, function(){
                            addCoin(player.id - 1);
                        });
                    }
                }
            }
        },
        {
            id: 5,
            type: 'blue',
            name: 'Лес',
            cost: 3,
            dice: [5],
            playerHas: false,
            image: 'images/cards/blue/5_forest.png',
            position: {
                x: 0,
                y: 1,
            },
            aiPriority: 1,
            usagePriority: 2,
            usage: function(player) {
                addCoin(player.id - 1);
            }
        },
        {
            id: 6,
            type: 'blue',
            name: 'Шахта',
            cost: 6,
            dice: [9],
            playerHas: false,
            image: 'images/cards/blue/9_mine.png',
            position: {
                x: 1,
                y: 2,
            },
            aiPriority: 4,
            usagePriority: 2,
            usage: function(player) {
                var coins_delay = 0;
                for (var i = 0; i < 5; i++) {
                    coins_delay += 50;
                    $.wait(coins_delay + 50, function(){
                        addCoin(player.id - 1);
                    });
                }
            }
        },
        {
            id: 7,
            type: 'blue',
            name: 'Яблоневый сад',
            cost: 3,
            dice: [10],
            playerHas: false,
            image: 'images/cards/blue/10_apple_garden.png',
            position: {
                x: 3,
                y: 2,
            },
            aiPriority: 1,
            usagePriority: 2,
            usage: function(player) {
                var coins_delay = 0;
                for (var i = 0; i < 3; i++) {
                    coins_delay += 50;
                    $.wait(coins_delay + 50, function(){
                        addCoin(player.id - 1);
                    });
                }
            }
        },
        {
            id: 8,
            type: 'red',
            name: 'Кафе',
            cost: 2,
            dice: [3],
            playerHas: false,
            image: 'images/cards/red/3_caffee.png',
            position: {
                x: 3,
                y: 0,
            },
            aiPriority: 1,
            usagePriority: 1,
            usage: function(player) {
                if (player.id - 1 != currentPlayer) {
                    var total_coins = 1;
                    if (player.flag_center) total_coins++;

                    var coins_delay = 0;
                    for (var i = 0; i < total_coins; i++) {
                        coins_delay += 50;
                        $.wait(coins_delay + 50, function(){
                            sendCoin(currentPlayer, player.id - 1);
                        });
                    }
                }
            }
        },
        {
            id: 9,
            type: 'red',
            name: 'Ресторан',
            cost: 3,
            dice: [9, 10],
            playerHas: false,
            image: 'images/cards/red/9_10_restauraunt.png',
            position: {
                x: 2,
                y: 2,
            },
            aiPriority: 1,
            usagePriority: 1,
            usage: function(player) {
                if (player.id - 1 != currentPlayer) {
                    var total_coins = 2;
                    if (player.flag_center) total_coins++;

                    var coins_delay = 0;
                    for (var i = 0; i < total_coins; i++) {
                        coins_delay += 50;
                        $.wait(coins_delay + 50, function(){
                            sendCoin(currentPlayer, player.id - 1);
                        });
                    }
                }
            }
        },
        {
            id: 10,
            type: 'green',
            name: 'Сыроварня',
            cost: 5,
            dice: [7],
            playerHas: false,
            image: 'images/cards/green/7_creamery.png',
            position: {
                x: 4,
                y: 1,
            },
            aiPriority: 1,
            usagePriority: 1,
            usage: function(player) {
                if (currentPlayer == player.id - 1) {
                    var total_coins = 0;
                    $.each(player.cards, function(k, card){
                        if (card.id == 3) {
                            total_coins += 3;
                        }
                    });

                    if (total_coins > 0 && currentPlayer == player.id - 1) {
                        var coins_delay = 0;
                        for (var i = 0; i < total_coins; i++) {
                            coins_delay += 50;
                            $.wait(coins_delay + 50, function(){
                                addCoin(player.id - 1);
                            });
                        }
                    }
                }
            }
        },
        {
            id: 11,
            type: 'green',
            name: 'Мебельная фабрика',
            cost: 3,
            dice: [8],
            playerHas: false,
            image: 'images/cards/green/8_furniture.png',
            position: {
                x: 0,
                y: 2,
            },
            aiPriority: 1,
            usagePriority: 1,
            usage: function(player) {
                if (currentPlayer == player.id - 1) {
                    var total_coins = 0;
                    $.each(player.cards, function(k, card){
                        if (card.id == 5 || card.id == 6) {
                            total_coins += 3;
                        }
                    });

                    if (total_coins > 0 && currentPlayer == player.id - 1) {
                        var coins_delay = 0;
                        for (var i = 0; i < total_coins; i++) {
                            coins_delay += 50;
                            $.wait(coins_delay + 50, function(){
                                addCoin(player.id - 1);
                            });
                        }
                    }
                }
            }
        },
        {
            id: 12,
            type: 'green',
            name: 'Фруктовый рынок',
            cost: 2,
            dice: [11, 12],
            playerHas: false,
            image: 'images/cards/green/11_12_market.png',
            position: {
                x: 4,
                y: 2,
            },
            aiPriority: 1,
            usagePriority: 2,
            usage: function(player) {
                if (currentPlayer == player.id - 1) {
                    var total_coins = 0;
                    $.each(player.cards, function(k, card){
                        if (card.id == 1 || card.id == 7) {
                            total_coins += 2;
                        }
                    });

                    if (total_coins > 0 && currentPlayer == player.id - 1) {
                        var coins_delay = 0;
                        for (var i = 0; i < total_coins; i++) {
                            coins_delay += 50;
                            $.wait(coins_delay + 50, function(){
                                addCoin(player.id - 1);
                            });
                        }
                    }
                }
            }
        },
        {
            id: 13,
            type: 'purple',
            name: 'Стадион',
            cost: 6,
            dice: [6],
            playerHas: false,
            image: 'images/cards/purple/6_stadium.png',
            position: {
                x: 2,
                y: 1,
            },
            aiPriority: 1,
            usagePriority: 3,
            usage: function(player) {
                if (currentPlayer == player.id - 1) {
                    $.each(players, function(k, player) {
                        if (k != currentPlayer) {
                            var total_coins = 2;
                            var coins_delay = 0;
                            for (var i = 0; i < total_coins; i++) {
                                coins_delay += 50;
                                $.wait(coins_delay + 50, function(){
                                    sendCoin(k, currentPlayer);
                                });
                            }
                        }
                    });
                }
            }
        },
        {
            id: 14,
            type: 'purple',
            name: 'Телецентр',
            cost: 7,
            dice: [6],
            playerHas: false,
            image: 'images/cards/purple/6_telecenter.png',
            position: {
                x: 3,
                y: 1,
            },
            aiPriority: 1,
            usagePriority: 4,
            usage: function(player) {
                if (currentPlayer == player.id - 1) {
                    $.wait(500, function(){
                        if (currentPlayer == 0) {
                            $('.telecenter .players_button').html('');
                            $.each(players, function(k, player) {
                                if (k != currentPlayer) {
                                    $('.telecenter .players_button').append('<div class="button" data-id="' + k + '">' + player.name + ' (монет: ' + player.coins + ')</div>');
                                }
                            });

                            $('.telecenter .players_button .button').click(function(){
                                var player_from = +$(this).attr('data-id');

                                log('<b>' + players[currentPlayer].name + '</b> забирает <b>5 монет</b> у <b>' + players[player_from].name + '</b>')

                                var total_coins = 5;
                                var coins_delay = 0;
                                for (var i = 0; i < total_coins; i++) {
                                    coins_delay += 50;
                                    $.wait(coins_delay + 50, function(){
                                        sendCoin(player_from, currentPlayer);
                                    });
                                }

                                $('.telecenter').fadeOut();
                            });

                            $('.telecenter').fadeIn();
                        } else {
                            var totalPriority = 0;
                            $.each(players, function(k, player) {
                                if (k != currentPlayer) {
                                    totalPriority += player.coins;
                                }
                            });

                            if (totalPriority > 0) {
                                var player_random = Math.floor(Math.random() * totalPriority);
                                totalPriority = 0;
                                $.each(players, function(k, player) {
                                    totalPriority += player.coins;
                                    if (player_random < totalPriority) {
                                        log('<b>' + players[currentPlayer].name + '</b> забирает <b>5 монет</b> у <b>' + players[k].name + '</b>')

                                        var total_coins = 5;
                                        var coins_delay = 0;
                                        for (var i = 0; i < total_coins; i++) {
                                            coins_delay += 50;
                                            $.wait(coins_delay + 50, function(){
                                                sendCoin(k, currentPlayer);
                                            });
                                        }

                                        return false;
                                    }
                                });
                            }
                        }
                    });
                }
            }
        },
        {
            id: 15,
            type: 'purple',
            name: 'Деловой центр',
            cost: 8,
            dice: [6],
            playerHas: false,
            image: 'images/cards/purple/6_business_center.png',
            position: {
                x: 1,
                y: 1,
            },
            aiPriority: 100,
            usagePriority: 4,
            usage: function(player) {
                var card_from, card_to, card_from_name, card_to_name, player_from;
                if (currentPlayer == player.id - 1) {
                    $.wait(500, function () {
                        if (currentPlayer == 0) {
                            $('.business .players_button').html('');
                            $.each(players, function(k, player) {
                                if (k != currentPlayer) {
                                    $('.business .players_button').append('<div class="button" data-id="' + k + '">' + player.name + ' (кол-во: ' + player.cards.length + ')</div>');
                                }
                            });

                            $('.status_sights').hide();
                            $('.modal .status_cards').addClass('full');

                            $('.business .players_button .button').click(function(){
                                player_from = +$(this).attr('data-id');

                                $('.modal .status_cards h2').html('Выберите предприятие игрока «' + players[player_from].name + '»');

                                $('.modal.status .card').remove();
                                var arr_cards = [];
                                $.each(cards, function(k, v) {
                                    arr_cards.push({
                                        id: v.id,
                                        amount: 0
                                    });
                                });

                                $.each(players[player_from].cards, function(k, card) {
                                    $.each(arr_cards, function(k, v){
                                        if (v.id == card.id) v.amount += 1;
                                    });

                                });

                                $.each(arr_cards, function(k, v) {
                                    if (v.amount > 0) {
                                        $.each(cards, function(k ,c) {
                                            if (c.id == v.id && c.type != 'purple') {
                                                $('.status_cards').append('<div class="card" data-id="' + c.id + '" data-name="' + c.name + '" style="background-image: url(' + c.image + ')"><span>' + c.name + (v.amount > 1 ? ' (x' + v.amount + ')' : '') + '</span></div>');
                                            }
                                        });
                                    }
                                });

                                $('.modal .status_cards .card').click(function(){
                                    card_from = +$(this).attr('data-id');
                                    card_from_name = $(this).attr('data-name');
                                    $('.modal.status').fadeOut(function(){
                                        $('.modal.status .card').remove();
                                        $('.modal .status_cards h2').html('Выберите свое предприятие');

                                        var arr_cards = [];
                                        $.each(cards, function(k, v) {
                                            arr_cards.push({
                                                id: v.id,
                                                amount: 0
                                            });
                                        });

                                        $.each(players[currentPlayer].cards, function(k, card) {
                                            $.each(arr_cards, function(k, v){
                                                if (v.id == card.id) v.amount += 1;
                                            });

                                        });

                                        $.each(arr_cards, function(k, v) {
                                            if (v.amount > 0) {
                                                $.each(cards, function(k ,c) {
                                                    if (c.id == v.id && c.type != 'purple') {
                                                        $('.status_cards').append('<div class="card" data-id="' + c.id + '" data-name="' + c.name + '" style="background-image: url(' + c.image + ')"><span>' + c.name + (v.amount > 1 ? ' (x' + v.amount + ')' : '') + '</span></div>');
                                                    }
                                                });
                                            }
                                        });

                                        $('.modal .status_cards .card').click(function(){
                                            card_to = +$(this).attr('data-id');
                                            card_to_name = $(this).attr('data-name');
                                            $('.modal.status').fadeOut(function(){
                                                $('.status_sights').show();
                                                $('.modal .status_cards').removeClass('full');
                                                $('.modal .status_cards h2').html('Предприятия');
                                            });
                                            $('.business').fadeOut();

                                            var cache;
                                            $.each(players[player_from].cards, function(k, c1){
                                                if (c1.id == card_from) {
                                                    cache = c1;
                                                    players[player_from].cards.splice(k, 1);
                                                    return false;
                                                }
                                            });

                                            players[currentPlayer].cards.push(cache);

                                            $.each(players[currentPlayer].cards, function(k, c2){
                                                if (c2.id == card_to) {
                                                    cache = c2;
                                                    players[currentPlayer].cards.splice(k, 1);
                                                    return false;
                                                }
                                            });

                                            players[player_from].cards.push(cache);

                                            $.wait(200, function(){
                                                $('.cards .card.player_' + (player_from + 1) + '_stack[data-id="' + (card_from - 1) + '"]').last().removeClass('player_' + (player_from + 1) + '_stack').addClass('player_' + (currentPlayer + 1) + '_stack');
                                            });

                                            $.wait(300, function(){
                                                $('.cards .card.player_' + (currentPlayer + 1) + '_stack[data-id="' + (card_to - 1) + '"]').last().removeClass('player_' + (currentPlayer + 1) + '_stack').addClass('player_' + (player_from + 1) + '_stack');
                                            });

                                            log('<b>'+players[currentPlayer].name+'</b> меняет карту «<b>'+card_from_name+'</b>» на «<b>'+card_to_name+'</b>» у <b>'+players[player_from].name+'</b>');

                                        });

                                        $('.modal.status').fadeIn();

                                    });
                                });

                                $('.modal.status').fadeIn();
                            });

                            $('.business').fadeIn();
                        } else {
                            var playersPriority = 0;
                            var playerTarget = 0;
                            var card_from;
                            var card_from_name;
                            var card_to;
                            var card_to_name;

                            $.each(players, function(k, player) {
                                if (k != currentPlayer) {
                                    playersPriority += player.coins + player.cards.length;
                                }
                            });

                            if (playersPriority > 0) {
                                var player_random = Math.floor(Math.random() * playersPriority);

                                playersPriority = 0;
                                $.each(players, function(k, card) {
                                    if (k != currentPlayer) {
                                        playersPriority += player.coins + player.cards.length;
                                        if (player_random < playersPriority) {
                                            playerTarget = k;
                                            return false;
                                        }
                                    }
                                });
                            }

                            var type_random = Math.floor(Math.random() * 4);
                            var totalPriority = 0;
                            $.each(players, function(player_k, player) {
                                if (player_k != currentPlayer) {
                                    $.each(player.cards, function (k, card) {
                                        if (card.type != 'purple') {
                                            totalPriority += card.aiPriority + (player_k == playerTarget ? 50 : 0);
                                            if (type_random == 0 && card.type == 'blue') totalPriority += 3;
                                            if (type_random == 1 && card.type == 'green') totalPriority += 3;
                                            if (type_random == 2 && card.type == 'red') totalPriority += 3;
                                            if (type_random == 3 && card.type == 'purple') totalPriority += 3;
                                        }
                                    });
                                }
                            });

                            if (totalPriority > 0) {
                                var card_random = Math.floor(Math.random() * totalPriority);

                                totalPriority = 0;
                                $.each(players, function(player_k, player) {
                                    if (player_k != currentPlayer) {
                                        $.each(player.cards, function (k, card) {
                                            if (card.type != 'purple') {
                                                totalPriority += card.aiPriority + (player_k == playerTarget ? 50 : 0);
                                                if (type_random == 0 && card.type == 'blue') totalPriority += 3;
                                                if (type_random == 1 && card.type == 'green') totalPriority += 3;
                                                if (type_random == 2 && card.type == 'red') totalPriority += 3;
                                                if (type_random == 3 && card.type == 'purple') totalPriority += 3;

                                                if (card_random < totalPriority) {
                                                    player_from = player_k;
                                                    card_from = card.id;
                                                    card_from_name = card.name;
                                                    return false;
                                                }
                                            }
                                        });
                                    }
                                });
                            }

                            var totalPriority = 0;
                            $.each(players[currentPlayer].cards, function(k, card){
                                if (card.id != card_from) {
                                    totalPriority += (100 - card.aiPriority);
                                }
                            });

                            if (totalPriority > 0) {
                                var card_random = Math.floor(Math.random() * totalPriority);

                                var totalPriority = 0;
                                $.each(players[currentPlayer].cards, function(k, card){
                                    if (card.id != card_from) {
                                        totalPriority += (100 - card.aiPriority);
                                        if (card_random < totalPriority) {
                                            card_to = card.id;
                                            card_to_name = card.name;
                                            return false;
                                        }
                                    }
                                });
                            }

                            var cache;
                            $.each(players[player_from].cards, function(k, c1){
                                if (c1.id == card_from) {
                                    cache = c1;
                                    players[player_from].cards.splice(k, 1);
                                    return false;
                                }
                            });

                            players[currentPlayer].cards.push(cache);

                            $.each(players[currentPlayer].cards, function(k, c2){
                                if (c2.id == card_to) {
                                    cache = c2;
                                    players[currentPlayer].cards.splice(k, 1);
                                    return false;
                                }
                            });

                            players[player_from].cards.push(cache);

                            $.wait(200, function(){
                                $('.cards .card.player_' + (player_from + 1) + '_stack[data-id="' + (card_from - 1) + '"]').last().removeClass('player_' + (player_from + 1) + '_stack').addClass('player_' + (currentPlayer + 1) + '_stack');
                            });

                            $.wait(300, function(){
                                $('.cards .card.player_' + (currentPlayer + 1) + '_stack[data-id="' + (card_to - 1) + '"]').last().removeClass('player_' + (currentPlayer + 1) + '_stack').addClass('player_' + (player_from + 1) + '_stack');
                            });

                            log('<b>'+players[currentPlayer].name+'</b> меняет карту «<b>'+card_from_name+'</b>» на «<b>'+card_to_name+'</b>» у <b>'+players[player_from].name+'</b>');

                        }
                    });
                }
            }
        },
    ],
    sights = [
        {
            type: 'station',
            name: 'Вокзал',
            cost: 4,
            image: 'images/cards/sights/4_station_inactive.png',
            image_active: 'images/cards/sights/4_station.png',
            position: 0,
            buyed: false,
        },
        {
            type: 'center',
            name: 'Торговый центр',
            cost: 10,
            image: 'images/cards/sights/10_center_inactive.png',
            image_active: 'images/cards/sights/10_center.png',
            position: 1,
            buyed: false,
        },
        {
            type: 'park',
            name: 'Парк аттракционов',
            cost: 16,
            image: 'images/cards/sights/16_park_inactive.png',
            image_active: 'images/cards/sights/16_park.png',
            position: 2,
            buyed: false,
        },
        {
            type: 'tower',
            name: 'Радиовышка',
            cost: 22,
            image: 'images/cards/sights/22_tower_inactive.png',
            image_active: 'images/cards/sights/22_tower.png',
            position: 3,
            buyed: false,
        },
    ],
    players = [],
    playersCount,
    currentPlayer,
    double = false,
    diceAttempts = 1,
    main = (function() {
        $(window).resize(function(){
            scaleViewPort();
        });
        $(window).trigger('resize');

        function scaleViewPort(){
            var w = window.innerWidth;
            var h = window.innerHeight;

            var scaleOfWidth = w/baseWidth;
            var scaleOfHeight = h/baseHeight;
            var newLeft = (w-(Math.min(scaleOfWidth, scaleOfHeight)*baseWidth))/2;
            var newTop = (h-(Math.min(scaleOfWidth, scaleOfHeight)*baseHeight))/2;
            var ratio = Math.min(scaleOfWidth, scaleOfHeight);

            $('.viewport').css({
                'zoom': ratio,
            });

            $(document).scrollLeft(0);
            $(document).scrollTop(0);
        }

        $.wait = function(duration, completeCallback, target) {
            var $target = $(target || '<queue />');
            return $target.delay(duration).queue(function(next){completeCallback && completeCallback.call($target); next();});
        }

        $.fn.wait = function(duration, completeCallback) {
            return $.wait.call(this, duration, completeCallback, this);
        };



        $('.start .button').click(function(){
            playersCount = +$('.start #players_count').val();
            $('.start').fadeOut();
            $('.disable_all').show();

            for (var player_id = 1; player_id <= playersCount; player_id++) {
                players.push({
                    id: player_id,
                    name: 'Игрок ' + player_id,
                    cards: [],
                    sights: [],
                    coins: 0,
                    flag_station: false,
                    flag_center: false,
                    flag_park: false,
                    flag_tower: false,
                });
            }

            $.each(players, function(k, player) {
                if (player.id > 1) {
                    $('.players_panels').append('<div class="panel" data-player-id="' + player.id + '"> ' +
                        '<span class="name">' + player.name + '</span>' +
                        '<span class="coins copper">' + player.coins + '</span>' +
                        '<div class="sights"></div>' +
                        '</div>');
                } else {
                    $('.your_panel').append('<div class="panel" data-player-id="' + player.id + '"> ' +
                        '<span class="name">' + player.name + '</span>' +
                        '<span class="coins copper">' + player.coins + '</span>' +
                        '<div class="sights"></div>' +
                        '</div>');
                }

                $.each(sights, function(k, sight) {
                    $('.panel[data-player-id="' + player.id + '"] .sights').append('<div class="card pos_' + sight.position + '" data-id="' + k + '" style="background-image: url(' + sight.image + ')"></div>');
                    player.sights.push(sight);
                });
            });

            $.each(cards, function(k, card) {
                if (card.playerHas) card.available = 5 + playersCount;
                else if (card.type == 'purple') card.available = playersCount;
                else card.available = 5;

                for (var i = 0; i < card.available; i++) {
                    $('.cards').append('<div class="card start_' + card.position.x + '_' + card.position.y + '" data-name="' + card.name + '"  data-id="' + k + '" style="background-image: url(' + card.image + ')"></div>');
                }
            });

            var delay = 0;
            $.each(cards, function(card_id, card) {
                $.each(players, function(k, player){
                    if (card.playerHas) {
                        delay += 200;
                        $.wait(delay + 200, function() {
                            $('.cards .card[data-id="' + card_id + '"]:not(.stack)').last().addClass('stack').addClass('player_' + player.id + '_stack');
                            card.available -= 1;
                            player.cards.push(card);
                        });
                    }
                });
            });

            var coins_delay = 0;
            for(var coin = 0; coin < 10; coin++) {
                $.each(players, function(k ,player) {
                    coins_delay += 50;
                    $.wait(coins_delay + 50, function(){
                        addCoin(k);
                    });
                });
            }

            log('<b>ИГРА НАЧАЛАСЬ</b>');

            $.wait(2000, function(){
                currentPlayer = -1;
                nextTurn();
            });

            $('.cards .card').click(function(){
                if ($('.cards').hasClass('can_buy') && !$(this).hasClass('disabled')) {
                    var card_id = +$(this).attr('data-id');
                    if (cards[card_id].cost <= players[currentPlayer].coins) {
                        $('.disable_all').show();
                        log('<b>' + players[currentPlayer].name + '</b> купил предприятие «<b>' + cards[card_id].name + '</b>»');
                        $(this).addClass('stack').addClass('player_' + players[currentPlayer].id + '_stack');

                        var coins_delay = 0;
                        for (var i = 0; i < cards[card_id].cost; i++) {
                            coins_delay += 50;
                            $.wait(coins_delay + 50, function(){
                                removeCoin(currentPlayer);
                            });
                        }

                        $.wait(coins_delay + 500, function(){
                            cards[card_id].available -= 1;
                            players[currentPlayer].cards.push(cards[card_id]);

                            $('.cards .card').removeClass('disabled');
                            nextTurn();
                        });
                    }
                }
            });

            $('.your_panel .card').click(function(){
                if ($('.cards').hasClass('can_buy')) {
                    var card_id = +$(this).attr('data-id');
                    if (sights[card_id].cost <= players[currentPlayer].coins && !$(this).hasClass('active')) {
                        $('.disable_all').show();
                        log('<b>' + players[currentPlayer].name + '</b> купил достопримечательность «<b>' + sights[card_id].name + '</b>»');
                        $(this).addClass('active').attr('style', 'background-image: url(' + sights[card_id].image_active + ')')

                        var coins_delay = 0;
                        for (var i = 0; i < sights[card_id].cost; i++) {
                            coins_delay += 50;
                            $.wait(coins_delay + 50, function(){
                                removeCoin(currentPlayer);
                            });
                        }

                        if (sights[card_id].type == 'station') players[currentPlayer].flag_station = true;
                        if (sights[card_id].type == 'center') players[currentPlayer].flag_center = true;
                        if (sights[card_id].type == 'park') players[currentPlayer].flag_park = true;
                        if (sights[card_id].type == 'tower') players[currentPlayer].flag_tower = true;

                        $.wait(coins_delay + 500, function(){
                            $.each(players[currentPlayer].sights, function(k, sight) {
                                if (k == card_id) {
                                    sight.buyed = true;
                                }
                            });

                            $('.cards .card').removeClass('disabled');
                            nextTurn();
                        });
                    }
                }
            });



            $('.panel .name').click(function() {
                var player_id = +$(this).parent().attr('data-player-id') - 1;
                $('.modal.status .card').remove();
                $.each(players[player_id].sights, function(k, sight) {
                    $('.status_sights').append('<div class="card" style="background-image: url(' + (sight.buyed == true ? sight.image_active : sight.image) + ')"><span>' + sight.name + '</span></div>');
                });

                var arr_cards = [];
                $.each(cards, function(k, v) {
                    arr_cards.push({
                        id: v.id,
                        amount: 0
                    });
                });

                $.each(players[player_id].cards, function(k, card) {
                    $.each(arr_cards, function(k, v){
                        if (card && v.id == card.id) v.amount += 1;
                    });

                });

                $.each(arr_cards, function(k, v) {
                    if (v.amount > 0) {
                        $.each(cards, function(k ,c) {
                            if (c.id == v.id) {
                                $('.status_cards').append('<div class="card" style="background-image: url(' + c.image + ')"><span>' + c.name + (v.amount > 1 ? ' (x' + v.amount + ')' : '') + '</span></div>');
                            }
                        });
                    }
                });

                $('.modal.status').fadeIn();

            });

        });

        function nextTurn() {
            $.each(players, function(k, player){
                player.cards.sort(function(a, b){
                    var keyA = a.usagePriority,
                        keyB = b.usagePriority;
                    if(keyA < keyB) return -1;
                    if(keyA > keyB) return 1;
                    return 0;
                })
            });

            if (currentPlayer < 0 || !players[currentPlayer].flag_park || !double) {
                currentPlayer++;
                if (currentPlayer >= playersCount) currentPlayer = 0;
            }

            if (players[currentPlayer].flag_tower && diceAttempts == 0) diceAttempts = 2;
            else diceAttempts = 1;

            $('.status_sights').show();

            $('.dices .normal').show();
            $('.dices .tower_buttons').hide();
            $('.cards .card').removeClass('disabled');

            $('.pass').css('opacity', 0);
            log('Ходит <b>' + players[currentPlayer].name + '</b>');

            if (players[currentPlayer].flag_station) $('.dice i:last-child').removeClass('disabled');
            else $('.dice i:last-child').addClass('disabled');

            if (currentPlayer == 0) {
                $('.dices h2').html('Ваш ход');
                $('.dices h2, .dices .button').show();
                $('.dices').fadeIn(function(){
                    $('.disable_all').hide();
                });
            } else {
                $('.dices h2').html('Ходит ' + players[currentPlayer].name);
                $('.dices .button').hide();
                $('.dices').fadeIn();
                $.wait(500, function() {
                    diceAI();
                });
            }
        }

        $('.dices .button').click(function(){
            if ($('.dice i.inactive').length != 2 && $(this).attr('id') != 'apply') {
                $('.disable_all').show();
                diceAttempts--;
                var dices_count = 0;
                var random_1 = Math.floor(Math.random() * 50) + 10,
                    random_dice_1, random_dice_2;

                for (var i = 0; i < random_1; i++) {
                    $.wait((i*30) + 30, function() {
                        random_dice_1 = Math.floor(Math.random() * 6) + 1;
                        random_dice_2 = Math.floor(Math.random() * 6) + 1;

                        if (!$('.dice i:first-child').hasClass('inactive')) {
                            $('.dice i:first-child')[0].classList.remove("pin1");
                            $('.dice i:first-child')[0].classList.remove("pin2");
                            $('.dice i:first-child')[0].classList.remove("pin3");
                            $('.dice i:first-child')[0].classList.remove("pin4");
                            $('.dice i:first-child')[0].classList.remove("pin5");
                            $('.dice i:first-child')[0].classList.remove("pin6");

                            $('.dice i:first-child')[0].classList.add("pin" + random_dice_1);
                        }

                        if (!$('.dice i:last-child').hasClass('inactive')) {
                            $('.dice i:last-child')[0].classList.remove("pin1");
                            $('.dice i:last-child')[0].classList.remove("pin2");
                            $('.dice i:last-child')[0].classList.remove("pin3");
                            $('.dice i:last-child')[0].classList.remove("pin4");
                            $('.dice i:last-child')[0].classList.remove("pin5");
                            $('.dice i:last-child')[0].classList.remove("pin6");

                            $('.dice i:last-child')[0].classList.add("pin" + random_dice_2);
                        }
                    });
                }

                $.wait(random_1*30 + 500, function(){
                    var result = (!$('.dice i:first-child').hasClass('inactive') ? random_dice_1 : 0) + (!$('.dice i:last-child').hasClass('disabled') && !$('.dice i:last-child').hasClass('inactive') ? random_dice_2 : 0);
                    log('<b>' + players[currentPlayer].name + '</b> выбросил <b>' + result + '</b>');

                    if (!$('.dice i:first-child').hasClass('inactive') && !$('.dice i:last-child').hasClass('disabled') && !$('.dice i:last-child').hasClass('inactive') && random_dice_1 == random_dice_2) double = true;

                    if (diceAttempts > 0) {
                        $('.disable_all').hide();
                        $('.dices .normal').hide();
                        $('.dices .tower_buttons').show();
                        $('#apply').attr('data-result', result);
                    } else {
                        useDiceResult(result);
                    }
                });
            } else if ($(this).attr('id') == 'apply') {
                var result = +$(this).attr('data-result');
                useDiceResult(result);
            }
        });

        function useDiceResult(result) {
            $('.dices').fadeOut();
            var result_delay = 0;
            $.each(players, function(k, player) {
                $.each(player.cards, function(k, card) {
                    $.each(card.dice, function(k, dice) {
                        if (dice == result) {
                            result_delay += 100;
                            $.wait(result_delay + 100, function() {
                                card.usage(player);
                            });
                        }
                    });
                });
            });

            $.wait(result_delay + 1000, function(){
                $('.cards').addClass('can_buy');
                $('.cards .card').each(function(){
                    var obj = $(this);
                    var card_id = +$(this).attr('data-id');
                    if (cards[card_id].cost > players[currentPlayer].coins) $(this).addClass('disabled');
                    if (cards[card_id].type == 'purple') {
                        $.each(players[currentPlayer].cards, function(k, c){
                            if (cards[card_id].id == c.id) {
                                obj.addClass('disabled');
                            }
                        });
                    }
                });

                if (currentPlayer == 0) {
                    $('.disable_all').hide();
                    $('.pass').css('opacity', 1);
                } else {
                    $.wait(100, function(){
                        buyAI();
                    });
                }

            });
        }

        function diceAI() {
            // if ($('.dice i.inactive').length != 2 && $(this).attr('id') != 'apply') {
            //     $('.disable_all').show();
                diceAttempts--;
                var dices_count = 0;
                var random_1 = Math.floor(Math.random() * 50) + 10,
                    random_dice_1, random_dice_2;

                for (var i = 0; i < random_1; i++) {
                    $.wait((i*30) + 30, function() {
                        random_dice_1 = Math.floor(Math.random() * 6) + 1;
                        random_dice_2 = Math.floor(Math.random() * 6) + 1;

                        if (!$('.dice i:first-child').hasClass('inactive')) {
                            $('.dice i:first-child')[0].classList.remove("pin1");
                            $('.dice i:first-child')[0].classList.remove("pin2");
                            $('.dice i:first-child')[0].classList.remove("pin3");
                            $('.dice i:first-child')[0].classList.remove("pin4");
                            $('.dice i:first-child')[0].classList.remove("pin5");
                            $('.dice i:first-child')[0].classList.remove("pin6");

                            $('.dice i:first-child')[0].classList.add("pin" + random_dice_1);
                        }

                        if (!$('.dice i:last-child').hasClass('inactive')) {
                            $('.dice i:last-child')[0].classList.remove("pin1");
                            $('.dice i:last-child')[0].classList.remove("pin2");
                            $('.dice i:last-child')[0].classList.remove("pin3");
                            $('.dice i:last-child')[0].classList.remove("pin4");
                            $('.dice i:last-child')[0].classList.remove("pin5");
                            $('.dice i:last-child')[0].classList.remove("pin6");

                            $('.dice i:last-child')[0].classList.add("pin" + random_dice_2);
                        }
                    });
                }

                $.wait(random_1*30 + 500, function(){
                    var result = (!$('.dice i:first-child').hasClass('inactive') ? random_dice_1 : 0) + (!$('.dice i:last-child').hasClass('disabled') && !$('.dice i:last-child').hasClass('inactive') ? random_dice_2 : 0);
                    log('<b>' + players[currentPlayer].name + '</b> выбросил <b>' + result + '</b>');

                    if (!$('.dice i:first-child').hasClass('inactive') && !$('.dice i:last-child').hasClass('disabled') && !$('.dice i:last-child').hasClass('inactive') && random_dice_1 == random_dice_2) double = true;

                    if (diceAttempts > 0) {
                        $('.disable_all').hide();
                        $('.dices .normal').hide();
                        $('.dices .tower_buttons').show();
                        $('#apply').attr('data-result', result);
                    } else {
                        useDiceResult(result);
                    }
                });
            // } else if ($(this).attr('id') == 'apply') {
            //     var result = +$(this).attr('data-result');
            //     useDiceResult(result);
            // }
        }

        function buyAI() {
            $('.cards .card').removeClass('disabled');

            var available_cards = [];
            $.each(cards, function(k, card){
                if (card.available > 0 && card.cost <= players[currentPlayer].coins) {
                    var flag_available = true;

                    if (card.type == 'purple') {
                        $.each(players[currentPlayer].cards, function(k, c){
                            if (card.id == c.id) flag_available = false;
                        });
                    }

                    if (flag_available) {
                        available_cards.push(card);
                    }
                }
            });

            var type_random = Math.floor(Math.random() * 4);
            var totalPriority = 0;
            $.each(available_cards, function(k, card) {
                totalPriority += card.aiPriority;
                if (type_random == 0 && card.type == 'blue') totalPriority += 3;
                if (type_random == 1 && card.type == 'green') totalPriority += 3;
                if (type_random == 2 && card.type == 'red') totalPriority += 3;
                if (type_random == 3 && card.type == 'purple') totalPriority += 3;
            });

            if (totalPriority > 0) {
                var card_random = Math.floor(Math.random() * totalPriority);

                totalPriority = 0;
                $.each(available_cards, function(k, card) {
                    totalPriority += card.aiPriority;
                    if (type_random == 0 && card.type == 'blue') totalPriority += 3;
                    if (type_random == 1 && card.type == 'green') totalPriority += 3;
                    if (type_random == 2 && card.type == 'red') totalPriority += 3;
                    if (type_random == 3 && card.type == 'purple') totalPriority += 3;

                    if (card_random < totalPriority) {
                        $('.cards .card[data-id="' + (card.id - 1) + '"]:not(.stack)').last().trigger('click');
                        return false;
                    }
                });
            } else {
                nextTurn();
            }
        }

        $('.modal.status .button').click(function(){
            $('.modal.status').fadeOut();
        });

        $('.pass').click(function(){
            nextTurn();
        });

        $('.dice i').click(function(){
            if(!$('.dice i:last-child').hasClass('disabled')) $(this).toggleClass('inactive');
        });

        $('.log').click(function(){
            console.log(cards);
            console.log(players);
        });

    })();

function addCoin(player_key) {
    var c = $('<div class="coin"></div>');
    $('.viewport').append(c);
    $.wait(100, function(){
        c.addClass('player_' + players[player_key].id + '_stack');
        $.wait(600, function(){
            players[player_key].coins += 1;

            var obj = $('.panel[data-player-id="' + players[player_key].id + '"] .coins');

            obj[0].classList.remove("pulse");
            void obj[0].offsetWidth;
            obj[0].classList.add("pulse");

            obj.html(players[player_key].coins);

            if (players[player_key].coins > 4) {
                obj[0].classList.remove("copper");
                obj[0].classList.add("silver");
            }

            if (players[player_key].coins > 9) {
                obj[0].classList.remove("silver");
                obj[0].classList.add("gold");
            }

            $.wait(1000, function(){
                c.remove();
            });

        });
    });
}

function removeCoin(player_key) {
    var c = $('<div class="coin player_' + players[player_key].id + '_stack"></div>');
    $('.viewport').append(c);
    $.wait(100, function(){
        c.removeClass('player_' + players[player_key].id + '_stack');
        $.wait(100, function(){
            players[player_key].coins -= 1;

            var obj = $('.panel[data-player-id="' + players[player_key].id + '"] .coins');

            obj[0].classList.remove("pulse");
            void obj[0].offsetWidth;
            obj[0].classList.add("pulse");

            obj.html(players[player_key].coins);

            if (players[player_key].coins < 5) {
                obj[0].classList.remove("silver");
                obj[0].classList.remove("gold");
                obj[0].classList.add("copper");
            }

            if (players[player_key].coins > 4) {
                obj[0].classList.remove("copper");
                obj[0].classList.remove("gold");
                obj[0].classList.add("silver");
            }

            if (players[player_key].coins > 9) {
                obj[0].classList.remove("copper");
                obj[0].classList.remove("silver");
                obj[0].classList.add("gold");
            }

            $.wait(1000, function(){
                c.remove();
            });

        });
    });
}

function sendCoin(player_from, player_to) {
    if (players[player_from].coins > 0) {
        players[player_from].coins -= 1;
        players[player_to].coins += 1;

        var c = $('<div class="coin player_' + players[player_from].id + '_stack"></div>');
        $('.viewport').append(c);
        $.wait(100, function(){
            c.removeClass('player_' + players[player_from].id + '_stack');
            c.addClass('player_' + players[player_to].id + '_stack');
            $.wait(100, function(){

                var obj = $('.panel[data-player-id="' + players[player_from].id + '"] .coins');

                obj[0].classList.remove("pulse");
                void obj[0].offsetWidth;
                obj[0].classList.add("pulse");

                obj.html(players[player_from].coins);

                if (players[player_from].coins < 5) {
                    obj[0].classList.remove("silver");
                    obj[0].classList.remove("gold");
                    obj[0].classList.add("copper");
                }

                if (players[player_from].coins > 4) {
                    obj[0].classList.remove("copper");
                    obj[0].classList.remove("gold");
                    obj[0].classList.add("silver");
                }

                if (players[player_from].coins > 9) {
                    obj[0].classList.remove("copper");
                    obj[0].classList.remove("silver");
                    obj[0].classList.add("gold");
                }

                var obj = $('.panel[data-player-id="' + players[player_to].id + '"] .coins');

                obj[0].classList.remove("pulse");
                void obj[0].offsetWidth;
                obj[0].classList.add("pulse");

                obj.html(players[player_to].coins);

                if (players[player_to].coins < 5) {
                    obj[0].classList.remove("silver");
                    obj[0].classList.remove("gold");
                    obj[0].classList.add("copper");
                }

                if (players[player_to].coins > 4) {
                    obj[0].classList.remove("copper");
                    obj[0].classList.remove("gold");
                    obj[0].classList.add("silver");
                }

                if (players[player_to].coins > 9) {
                    obj[0].classList.remove("copper");
                    obj[0].classList.remove("silver");
                    obj[0].classList.add("gold");
                }

                $.wait(1000, function(){
                    c.remove();
                });

            });
        });
    }
}

function log(str) {
    $('.log').append('<p>' + str + '</p>');
    var wtf    = $('.log');
    var height = wtf[0].scrollHeight;
    wtf.scrollTop(height);
}