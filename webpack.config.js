'use strict';

const gulpConfig = require('./gulpconfig');
const path = require('path');
const webpack = require('webpack');

let entryValue = {
    desktop: './common',
    mobile: './common-mobile'
}

if (!gulpConfig.mobile) {
    entryValue = {
        desktop: './common'
    }
}

module.exports = function(env) {
    const config = {
        context: path.resolve(__dirname, './assets'),
        entry: entryValue,
        output: {
            path: path.resolve(__dirname, './public'),
            filename: '[name].bundle.js',
            library: '[name]',
            publicPath: '/'
        },
        module: {
            rules: [
                {
                    test: /\.js$/,
                    include: path.resolve(__dirname, './assets'),
                    loader: 'babel-loader',
                    options: {
                        presets: [
                            'es2015'
                        ]
                    }
                }
            ]
        },
        resolve: {
            extensions: ['.*', '.js'],
            modules: [
                path.resolve(__dirname, './public/node_modules'),
                path.resolve(__dirname, './assets/blocks'),
                'node_modules'
            ]
        },
        plugins: [
            new webpack.optimize.UglifyJsPlugin()
        ]
    };

    return config;
};