'use strict';

const config = require('../gulpconfig'),
    browserSync = require('browser-sync'),
    fileinclude = require('gulp-file-include'),
    gulp = require('gulp'),
    htmlhint = require('gulp-htmlhint');

gulp.task('htmlBuild', function() {
    var fileincludeOptions = {
            prefix: '@@',
            basepath: '@file',
            indent: true
        };

    return gulp.src(config.app + '/templates/*.html')
        .pipe(fileinclude(fileincludeOptions))
        .pipe(gulp.dest(config.dist))
        .pipe(browserSync.stream());
});

// Rules https://github.com/yaniswang/HTMLHint/wiki/Rules
gulp.task('htmlHint', function() {
    var htmlhintOptions = {
            'doctype-first': false
        };

    return gulp.src([
            config.app + '/templates/**/*.html',
            '!' + config.app + '/templates/include/header.html',
            '!' + config.app + '/templates/include/footer.html',
            '!' + config.app + '/templates/include-mobile/header.html',
            '!' + config.app + '/templates/include-mobile/footer.html'
        ])
        .pipe(htmlhint(htmlhintOptions))
        .pipe(htmlhint.reporter())
});

gulp.task('templates', ['htmlBuild', 'htmlHint']);


// 'use strict';
//
// const config = require('../gulpconfig'),
//     browserSync = require('browser-sync'),
//     gutil = require('gulp-util'),
//     gulp = require('gulp'),
//     jade = require('gulp-jade');
//
// gulp.task('jade', function() {
//     const jadeOptions = {
//         doctype: 'html',
//         pretty: '    '
//     };
//
//     return gulp.src(config.app + '/jade/*.jade')
//         .pipe(jade(jadeOptions).on('error', function(err) {
//             gutil.log(gutil.colors.red(err.message));
//             gutil.beep();
//             this.emit('end');
//         }))
//         .pipe(gulp.dest(config.dist))
//         .pipe(browserSync.stream());
// });
//
// gulp.task('templates', ['jade']);
