'use strict';

const config = require('../gulpconfig'),
    changed = require('gulp-changed'),
    gulp = require('gulp'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant');

gulp.task('images', function() {
    const imageminOptions = {
            progressive: true,
            svgoPlugins: [{
                removeViewBox: false
            }],
            use: [pngquant()]
        };

    return gulp.src([config.dist + '/images/**/*.{png,jpg,gif,svg}'])
        // .pipe(changed(config.dist + '/images/'))
        .pipe(imagemin(imageminOptions))
        .pipe(gulp.dest(config.dist + '/images/'));
});
